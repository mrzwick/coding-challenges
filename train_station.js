const fs= require('fs');

const solution = (start, end, limit) => {
  const entryFee = 1;
  const stationFee = 2;
  let fee = 0;

  // Get the highest station number from the trips to determine the maximum daily fee
  const maxStation = Math.max(Math.max(...start), Math.max(...end));
  const maxFee = limit[maxStation];

  // Check for bad maximumFee inputs based on the trip
  if (maxFee === undefined) {
    console.error(`Cannot determine maxFee for out of bounds station ${maxStation}`);
    return -1;
  }

  console.debug(`Maximum fee is ${maxFee} for station ${maxStation}`);

  for (let i = 0; i < start.length; i++) {
    const numberStops = Math.abs(start[i] - end[i]);
    const tripFee = entryFee + (stationFee * numberStops);
    console.debug(`Started at ${start[i]}, Ended at ${end[i]}. Charging fee of ${tripFee}`);
    fee += tripFee;

    // Return early if we've reached the maxFee for the day
    if (fee >= maxFee) {
      return maxFee;
    }
  }

  return fee;
}

const loadTestCases = (filePath) => {
  try {
    const rawData = fs.readFileSync(filePath);
    return JSON.parse(rawData);
  } catch (error) {
    console.error(`Error reading file: ${error}`);
    return null;
  }
};

const runTestCases = () => {
  const args = process.argv;
  if (args.length < 3) {
    console.error('Please provide a file path for test cases.');
    return;
  }

  const filePath = args[2];
  const testData = loadTestCases(filePath);
  if (testData) {
    console.debug(`Start: ${testData.start}`);
    console.debug(`End: ${testData.end}`);
    console.debug(`Limit: ${testData.limit}`);

    const answer = solution(testData.start, testData.end, testData.limit);
    if (answer !== testData.expected) {
      console.error(`Expected ${testData.expected}, got ${answer}`);
    } else {
      console.info(`Got expected answer of ${answer}`);
    }
  }
};

runTestCases();